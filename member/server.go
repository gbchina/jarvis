//
// 用户中心服务
// 负责用户客户端 HTTPServer、Websocket服务简历
//

package member

import (
	"adai.design/jarvis/server"
	"net/http"
)

// 用户中心业务路由表
var router = map[string]http.Handler{}

type memberHTTP struct{}

func (*memberHTTP) ServeHTTP(response http.ResponseWriter, request *http.Request) {
	err := request.ParseForm()
	if err != nil {
		return
	}

	if request.URL.Path == "/member" {
		serveHTTP(response, request)
		return
	} else if request.URL.Path == "/member/ws" {
		workers.ServeHTTP(response, request)
		return
	} else {
		http.Error(response, "404 Not Found ^_^", 404)
		return
	}

}

func init() {
	server.AddService("member", &memberHTTP{})
}

func Start(end chan bool) {
}
