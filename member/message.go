//
// 用户中心与客户端消息通信格式
//

package member

import (
	"adai.design/jarvis/common/log"
	"encoding/json"
)

type Message struct {
	Path    string          `json:"path"`
	Method  string          `json:"method,omitempty"`
	Home    string          `json:"home,omitempty"`
	Session string          `json:"token,omitempty"`
	State   string          `json:"state,omitempty"`
	Data    json.RawMessage `json:"data,omitempty"`
}

func (m *Message) String() string {
	buf, _ := json.Marshal(m)
	return string(buf)
}

// 消息类型
const (
	msgPathLogin      = "member/login"
	msgPathLogout     = "member/logout"
	msgPathMemberAPNS = "member/push/apns"

	MsgMethodPost = "post"
	MsgMethodGet  = "get"
	MsgMethodPut  = "put"
)

// 用户中心与家庭中心通信消息类型
type MessagePkg struct {
	Type int      `json:"type"`
	Ctx  *Context `json:"context"`
	Msg  *Message `json:"msg"`
}

type Context struct {
	HomeId   string           `json:"home"`
	MemberId string           `json:"member"`
	Session  string           `json:"session"`
	Result   chan *MessagePkg `json:"-"`
}

func (p *MessagePkg) String() string {
	buf, err := json.Marshal(p)
	if err != nil {
		log.Fatal("%s", err)
	}
	return string(buf)
}

const (
	PkgTypeChanel  = 1 // 家庭中心通过chanel返回处理结果
	PkgTypeContext = 2 // 家庭中心通过上下文返回处理结果
)
