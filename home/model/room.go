//
// Create by Zeng Yun on 2018/12/22
//

package model

type Room struct {
	Id   string `json:"id" bson:"id"`
	Name string `json:"name" bson:"name"`
}
