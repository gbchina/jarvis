//
// Create by Zeng Yun on 2018/12/20
//
// 家庭成员
//

package model

const (
	MemberStateUnknown = ""
	MemberStateArrive  = "arrive"
	MemberStateLeave   = "leave"

	MemberRoleAdmin = "admin"
	MemberRoleGuest = "guest"
)

type Member struct {
	Id    string `json:"id" bson:"id"`
	Role  string `json:"role" bson:"role"`
	State string `json:"-" bson:"-"`
}
