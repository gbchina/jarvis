package main

import (
	"adai.design/jarvis/common/log"
	"adai.design/jarvis/device"
	"adai.design/jarvis/home"
	"adai.design/jarvis/member"
	"adai.design/jarvis/server"
)

func main() {
	log.Info("let's go!")
	end := make(chan bool, 1)

	server.Start(end)
	device.Start(end)
	member.Start(end)
	home.Start()

	<-end
}
